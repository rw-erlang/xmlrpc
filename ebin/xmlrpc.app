{application,xmlrpc,
 [{description,"xmlrpc"},
  {vsn,"1.14"},
  {modules,[xmlrpc, xmlrpc_decode, xmlrpc_encode, xmlrpc_http, xmlrpc_tcp_serv, xmlrpc_util]},
  {registered, []},
  {env, []},
  {applications,[kernel,stdlib]}]}.
